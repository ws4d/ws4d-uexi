#ifndef __1_SOAP_H__
#define __1_SOAP_H__


// ENUMERATION_1_space_3_NCName
#define EN_1_SPACE_3_NCNAME_NR0_DEFAULT 0
#define EN_1_SPACE_3_NCNAME_NR1_PRESERVE 1

void FSM_4_Body(int state);
void FSM_4_Code(int state);
void FSM_4_Detail(int state);
void FSM_4_Envelope(int state);
void FSM_4_Fault(int state);
void FSM_4_Header(int state);
void FSM_4_Node(int state);
void FSM_4_NotUnderstood(int state);
void FSM_4_Reason(int state);
void FSM_4_Role(int state);
void FSM_4_Subcode(int state);
void FSM_4_SupportedEnvelope(int state);
void FSM_4_Text(int state);
void FSM_4_Upgrade(int state);
void FSM_4_Value(int state);


void FSM_Helper();

void FSM_Start();

unsigned char eventCode;


#endif // __1_SOAP_H__
