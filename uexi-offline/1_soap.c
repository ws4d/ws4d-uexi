#include "1_soap.h"
#include "Processing.h"

//Strict Mode = true


/*	URI Table
	---------

	0:		
	1:		http://www.w3.org/XML/1998/namespace
	2:		http://www.w3.org/2001/XMLSchema-instance
	3:		http://www.w3.org/2001/XMLSchema
	4:		http://www.w3.org/2003/05/soap-envelope
*/


//FSM Helper function generated by EXIficient

void FSM_Helper() {

	#ifdef DEBUGMODE
	printf("\nDecoding QName...\n");
	#endif

	getQName();

	#ifdef DEBUGMODE
	printf("URI ID: %i\n", curr_uriID);
	printf("LocalName ID: %i\n\n", curr_lnID);
	#endif

	switch(curr_uriID)
	{
		case 0: //empty Namespace

			#ifdef DEBUGMODE
			printf("Namespace: empty\n");
			#endif

			switch(curr_lnID)
			{
				case 0: //qname
					break;

				default:
					break;
			}
			break;

		case 1: //http://www.w3.org/XML/1998/namespace

			#ifdef DEBUGMODE
			printf("Namespace: http://www.w3.org/XML/1998/namespace\n");
			#endif

			switch(curr_lnID)
			{
				case 0: //base
					#ifdef DEBUGMODE
					printf("Attribute LocalName: base\n");
					#endif
					datatype = STRING;
					getData();
					break;

				case 1: //id
					break;

				case 2: //lang
					#ifdef DEBUGMODE
					printf("Attribute LocalName: lang\n");
					#endif
					datatype = STRING;
					getData();
					break;

				case 3: //space
					#ifdef DEBUGMODE
					printf("Attribute LocalName: space\n");
					#endif
					datatype = ENUMERATION;
					enum_codelength = 1;
					getData();
					switch(enum_position)
					{
						case EN_1_SPACE_3_NCNAME_NR0_DEFAULT:
							#ifdef DEBUGMODE
							printf("Enumeration Value: 'EN_1_space_3_NCName_NR0_default'\n");
							#endif
							break;
						case EN_1_SPACE_3_NCNAME_NR1_PRESERVE:
							#ifdef DEBUGMODE
							printf("Enumeration Value: 'EN_1_space_3_NCName_NR1_preserve'\n");
							#endif
							break;
					}
					break;

				default:
					break;
			}
			break;

		case 2: //http://www.w3.org/2001/XMLSchema-instance

			#ifdef DEBUGMODE
			printf("Namespace: http://www.w3.org/2001/XMLSchema-instance\n");
			#endif

			switch(curr_lnID)
			{
				case 0: //nil
					break;

				case 1: //type
					break;

				default:
					break;
			}
			break;

		case 3: //http://www.w3.org/2001/XMLSchema

			#ifdef DEBUGMODE
			printf("Namespace: http://www.w3.org/2001/XMLSchema\n");
			#endif

			switch(curr_lnID)
			{
				case 0: //ENTITIES
					break;

				case 1: //ENTITY
					break;

				case 2: //ID
					break;

				case 3: //IDREF
					break;

				case 4: //IDREFS
					break;

				case 5: //NCName
					break;

				case 6: //NMTOKEN
					break;

				case 7: //NMTOKENS
					break;

				case 8: //NOTATION
					break;

				case 9: //Name
					break;

				case 10: //QName
					break;

				case 11: //anySimpleType
					break;

				case 12: //anyType
					break;

				case 13: //anyURI
					break;

				case 14: //base64Binary
					break;

				case 15: //boolean
					break;

				case 16: //byte
					break;

				case 17: //date
					break;

				case 18: //dateTime
					break;

				case 19: //decimal
					break;

				case 20: //double
					break;

				case 21: //duration
					break;

				case 22: //float
					break;

				case 23: //gDay
					break;

				case 24: //gMonth
					break;

				case 25: //gMonthDay
					break;

				case 26: //gYear
					break;

				case 27: //gYearMonth
					break;

				case 28: //hexBinary
					break;

				case 29: //int
					break;

				case 30: //integer
					break;

				case 31: //language
					break;

				case 32: //long
					break;

				case 33: //negativeInteger
					break;

				case 34: //nonNegativeInteger
					break;

				case 35: //nonPositiveInteger
					break;

				case 36: //normalizedString
					break;

				case 37: //positiveInteger
					break;

				case 38: //short
					break;

				case 39: //string
					break;

				case 40: //time
					break;

				case 41: //token
					break;

				case 42: //unsignedByte
					break;

				case 43: //unsignedInt
					break;

				case 44: //unsignedLong
					break;

				case 45: //unsignedShort
					break;

				default:
					break;
			}
			break;

		case 4: //http://www.w3.org/2003/05/soap-envelope

			#ifdef DEBUGMODE
			printf("Namespace: http://www.w3.org/2003/05/soap-envelope\n");
			#endif

			switch(curr_lnID)
			{
				case 0: //Body
					#ifdef DEBUGMODE
					printf("Element LocalName: Body\n");
					#endif
					FSM_4_Body(0);
					break;

				case 1: //Code
					break;

				case 2: //Detail
					break;

				case 3: //Envelope
					#ifdef DEBUGMODE
					printf("Element LocalName: Envelope\n");
					#endif
					FSM_4_Envelope(0);
					break;

				case 4: //Fault
					#ifdef DEBUGMODE
					printf("Element LocalName: Fault\n");
					#endif
					FSM_4_Fault(0);
					break;

				case 5: //Header
					#ifdef DEBUGMODE
					printf("Element LocalName: Header\n");
					#endif
					FSM_4_Header(0);
					break;

				case 6: //Node
					break;

				case 7: //NotUnderstood
					#ifdef DEBUGMODE
					printf("Element LocalName: NotUnderstood\n");
					#endif
					FSM_4_NotUnderstood(0);
					break;

				case 8: //NotUnderstoodType
					break;

				case 9: //Reason
					break;

				case 10: //Role
					break;

				case 11: //Subcode
					break;

				case 12: //SupportedEnvType
					break;

				case 13: //SupportedEnvelope
					break;

				case 14: //Text
					break;

				case 15: //Upgrade
					#ifdef DEBUGMODE
					printf("Element LocalName: Upgrade\n");
					#endif
					FSM_4_Upgrade(0);
					break;

				case 16: //UpgradeType
					break;

				case 17: //Value
					break;

				case 18: //detail
					break;

				case 19: //faultcode
					break;

				case 20: //faultcodeEnum
					break;

				case 21: //faultreason
					break;

				case 22: //reasontext
					break;

				case 23: //subcode
					break;

				default:
					break;
			}
			break;

		default:
			break;
	}
}


void FSM_4_Body(int state)
{

	#ifdef DEBUGMODE
	printf("\ncurrent FSM: 4_Body\n");
	#endif

	curr_uriID = 4;
	curr_lnID = 0;

	eventCode = 0;

	switch(state)
	{
		case 0:	//START_ELEMENT, 4_Body

			#ifdef DEBUGMODE
			printf("Event: START_ELEMENT\n");
			printf("Data: 4_Body\n");
			#endif

			getbit(&eventCode, 2);
			switch(eventCode)
			{
				case 0: state = 1;
					break;
				case 1: state = 2;
					break;
				case 2: state = 3;
					break;
			}
			FSM_4_Body(state);
			break;

		case 1:	//ATTRIBUTE_GENERIC, ATTRIBUTE_GENERIC

			#ifdef DEBUGMODE
			printf("Event: ATTRIBUTE_GENERIC\n");
			printf("Data: ATTRIBUTE_GENERIC\n");
			#endif

			FSM_Helper();

			getbit(&eventCode, 2);
			switch(eventCode)
			{
				case 0: state = 1;
					break;
				case 1: state = 2;
					break;
				case 2: state = 3;
					break;
			}
			FSM_4_Body(state);
			break;

		case 2:	//START_ELEMENT_GENERIC, START_ELEMENT_GENERIC

			#ifdef DEBUGMODE
			printf("Event: START_ELEMENT_GENERIC\n");
			printf("Data: START_ELEMENT_GENERIC\n");
			#endif

			FSM_Helper();

			getbit(&eventCode, 1);
			switch(eventCode)
			{
				case 0: state = 2;
					break;
				case 1: state = 3;
					break;
			}
			FSM_4_Body(state);
			break;

		case 3:	//END_ELEMENT, END_ELEMENT

			#ifdef DEBUGMODE
			printf("Event: END_ELEMENT\n");
			printf("Data: END_ELEMENT\n");
			#endif

			break;

		default:
			#ifdef DEBUGMODE
			printf("Undeclared, all stop!\n");
			#endif
			break;
	}
}

void FSM_4_Code(int state)
{

	#ifdef DEBUGMODE
	printf("\ncurrent FSM: 4_Code\n");
	#endif

	curr_uriID = 4;
	curr_lnID = 1;

	eventCode = 0;

	switch(state)
	{
		case 0:	//START_ELEMENT, 4_Code

			#ifdef DEBUGMODE
			printf("Event: START_ELEMENT\n");
			printf("Data: 4_Code\n");
			#endif

			state = 1;
			FSM_4_Code(state);
			break;

		case 1:	//START_ELEMENT, 4_Value

			#ifdef DEBUGMODE
			printf("Event: START_ELEMENT\n");
			printf("Data: 4_Value\n");
			#endif

			FSM_4_Value(0);

			getbit(&eventCode, 1);
			switch(eventCode)
			{
				case 0: state = 2;
					break;
				case 1: state = 3;
					break;
			}
			FSM_4_Code(state);
			break;

		case 2:	//START_ELEMENT, 4_Subcode

			#ifdef DEBUGMODE
			printf("Event: START_ELEMENT\n");
			printf("Data: 4_Subcode\n");
			#endif

			FSM_4_Subcode(0);

			state = 3;
			FSM_4_Code(state);
			break;

		case 3:	//END_ELEMENT, END_ELEMENT

			#ifdef DEBUGMODE
			printf("Event: END_ELEMENT\n");
			printf("Data: END_ELEMENT\n");
			#endif

			break;

		default:
			#ifdef DEBUGMODE
			printf("Undeclared, all stop!\n");
			#endif
			break;
	}
}

void FSM_4_Detail(int state)
{

	#ifdef DEBUGMODE
	printf("\ncurrent FSM: 4_Detail\n");
	#endif

	curr_uriID = 4;
	curr_lnID = 2;

	eventCode = 0;

	switch(state)
	{
		case 0:	//START_ELEMENT, 4_Detail

			#ifdef DEBUGMODE
			printf("Event: START_ELEMENT\n");
			printf("Data: 4_Detail\n");
			#endif

			getbit(&eventCode, 2);
			switch(eventCode)
			{
				case 0: state = 1;
					break;
				case 1: state = 2;
					break;
				case 2: state = 3;
					break;
			}
			FSM_4_Detail(state);
			break;

		case 1:	//ATTRIBUTE_GENERIC, ATTRIBUTE_GENERIC

			#ifdef DEBUGMODE
			printf("Event: ATTRIBUTE_GENERIC\n");
			printf("Data: ATTRIBUTE_GENERIC\n");
			#endif

			FSM_Helper();

			getbit(&eventCode, 2);
			switch(eventCode)
			{
				case 0: state = 1;
					break;
				case 1: state = 2;
					break;
				case 2: state = 3;
					break;
			}
			FSM_4_Detail(state);
			break;

		case 2:	//START_ELEMENT_GENERIC, START_ELEMENT_GENERIC

			#ifdef DEBUGMODE
			printf("Event: START_ELEMENT_GENERIC\n");
			printf("Data: START_ELEMENT_GENERIC\n");
			#endif

			FSM_Helper();

			getbit(&eventCode, 1);
			switch(eventCode)
			{
				case 0: state = 2;
					break;
				case 1: state = 3;
					break;
			}
			FSM_4_Detail(state);
			break;

		case 3:	//END_ELEMENT, END_ELEMENT

			#ifdef DEBUGMODE
			printf("Event: END_ELEMENT\n");
			printf("Data: END_ELEMENT\n");
			#endif

			break;

		default:
			#ifdef DEBUGMODE
			printf("Undeclared, all stop!\n");
			#endif
			break;
	}
}

void FSM_4_Envelope(int state)
{

	#ifdef DEBUGMODE
	printf("\ncurrent FSM: 4_Envelope\n");
	#endif

	curr_uriID = 4;
	curr_lnID = 3;

	eventCode = 0;

	switch(state)
	{
		case 0:	//START_ELEMENT, 4_Envelope

			#ifdef DEBUGMODE
			printf("Event: START_ELEMENT\n");
			printf("Data: 4_Envelope\n");
			#endif

			getbit(&eventCode, 2);
			switch(eventCode)
			{
				case 0: state = 1;
					break;
				case 1: state = 2;
					break;
				case 2: state = 3;
					break;
				case 3: state = 4;
					break;
			}
			FSM_4_Envelope(state);
			break;

		case 1:	//ATTRIBUTE_GENERIC, ATTRIBUTE_GENERIC

			#ifdef DEBUGMODE
			printf("Event: ATTRIBUTE_GENERIC\n");
			printf("Data: ATTRIBUTE_GENERIC\n");
			#endif

			FSM_Helper();

			getbit(&eventCode, 2);
			switch(eventCode)
			{
				case 0: state = 1;
					break;
				case 1: state = 2;
					break;
				case 2: state = 3;
					break;
				case 3: state = 4;
					break;
			}
			FSM_4_Envelope(state);
			break;

		case 2:	//START_ELEMENT, 4_Header

			#ifdef DEBUGMODE
			printf("Event: START_ELEMENT\n");
			printf("Data: 4_Header\n");
			#endif

			FSM_4_Header(0);

			state = 3;
			FSM_4_Envelope(state);
			break;

		case 3:	//START_ELEMENT, 4_Body

			#ifdef DEBUGMODE
			printf("Event: START_ELEMENT\n");
			printf("Data: 4_Body\n");
			#endif

			FSM_4_Body(0);

			state = 4;
			FSM_4_Envelope(state);
			break;

		case 4:	//END_ELEMENT, END_ELEMENT

			#ifdef DEBUGMODE
			printf("Event: END_ELEMENT\n");
			printf("Data: END_ELEMENT\n");
			#endif

			break;

		default:
			#ifdef DEBUGMODE
			printf("Undeclared, all stop!\n");
			#endif
			break;
	}
}

void FSM_4_Fault(int state)
{

	#ifdef DEBUGMODE
	printf("\ncurrent FSM: 4_Fault\n");
	#endif

	curr_uriID = 4;
	curr_lnID = 4;

	eventCode = 0;

	switch(state)
	{
		case 0:	//START_ELEMENT, 4_Fault

			#ifdef DEBUGMODE
			printf("Event: START_ELEMENT\n");
			printf("Data: 4_Fault\n");
			#endif

			state = 1;
			FSM_4_Fault(state);
			break;

		case 1:	//START_ELEMENT, 4_Code

			#ifdef DEBUGMODE
			printf("Event: START_ELEMENT\n");
			printf("Data: 4_Code\n");
			#endif

			FSM_4_Code(0);

			state = 2;
			FSM_4_Fault(state);
			break;

		case 2:	//START_ELEMENT, 4_Reason

			#ifdef DEBUGMODE
			printf("Event: START_ELEMENT\n");
			printf("Data: 4_Reason\n");
			#endif

			FSM_4_Reason(0);

			getbit(&eventCode, 2);
			switch(eventCode)
			{
				case 0: state = 3;
					break;
				case 1: state = 4;
					break;
				case 2: state = 5;
					break;
				case 3: state = 6;
					break;
			}
			FSM_4_Fault(state);
			break;

		case 3:	//START_ELEMENT, 4_Node

			#ifdef DEBUGMODE
			printf("Event: START_ELEMENT\n");
			printf("Data: 4_Node\n");
			#endif

			FSM_4_Node(0);

			getbit(&eventCode, 2);
			switch(eventCode)
			{
				case 0: state = 4;
					break;
				case 1: state = 5;
					break;
				case 2: state = 6;
					break;
			}
			FSM_4_Fault(state);
			break;

		case 4:	//START_ELEMENT, 4_Role

			#ifdef DEBUGMODE
			printf("Event: START_ELEMENT\n");
			printf("Data: 4_Role\n");
			#endif

			FSM_4_Role(0);

			getbit(&eventCode, 1);
			switch(eventCode)
			{
				case 0: state = 5;
					break;
				case 1: state = 6;
					break;
			}
			FSM_4_Fault(state);
			break;

		case 5:	//START_ELEMENT, 4_Detail

			#ifdef DEBUGMODE
			printf("Event: START_ELEMENT\n");
			printf("Data: 4_Detail\n");
			#endif

			FSM_4_Detail(0);

			state = 6;
			FSM_4_Fault(state);
			break;

		case 6:	//END_ELEMENT, END_ELEMENT

			#ifdef DEBUGMODE
			printf("Event: END_ELEMENT\n");
			printf("Data: END_ELEMENT\n");
			#endif

			break;

		default:
			#ifdef DEBUGMODE
			printf("Undeclared, all stop!\n");
			#endif
			break;
	}
}

void FSM_4_Header(int state)
{

	#ifdef DEBUGMODE
	printf("\ncurrent FSM: 4_Header\n");
	#endif

	curr_uriID = 4;
	curr_lnID = 5;

	eventCode = 0;

	switch(state)
	{
		case 0:	//START_ELEMENT, 4_Header

			#ifdef DEBUGMODE
			printf("Event: START_ELEMENT\n");
			printf("Data: 4_Header\n");
			#endif

			getbit(&eventCode, 2);
			switch(eventCode)
			{
				case 0: state = 1;
					break;
				case 1: state = 2;
					break;
				case 2: state = 3;
					break;
			}
			FSM_4_Header(state);
			break;

		case 1:	//ATTRIBUTE_GENERIC, ATTRIBUTE_GENERIC

			#ifdef DEBUGMODE
			printf("Event: ATTRIBUTE_GENERIC\n");
			printf("Data: ATTRIBUTE_GENERIC\n");
			#endif

			FSM_Helper();

			getbit(&eventCode, 2);
			switch(eventCode)
			{
				case 0: state = 1;
					break;
				case 1: state = 2;
					break;
				case 2: state = 3;
					break;
			}
			FSM_4_Header(state);
			break;

		case 2:	//START_ELEMENT_GENERIC, START_ELEMENT_GENERIC

			#ifdef DEBUGMODE
			printf("Event: START_ELEMENT_GENERIC\n");
			printf("Data: START_ELEMENT_GENERIC\n");
			#endif

			FSM_Helper();

			getbit(&eventCode, 1);
			switch(eventCode)
			{
				case 0: state = 2;
					break;
				case 1: state = 3;
					break;
			}
			FSM_4_Header(state);
			break;

		case 3:	//END_ELEMENT, END_ELEMENT

			#ifdef DEBUGMODE
			printf("Event: END_ELEMENT\n");
			printf("Data: END_ELEMENT\n");
			#endif

			break;

		default:
			#ifdef DEBUGMODE
			printf("Undeclared, all stop!\n");
			#endif
			break;
	}
}

void FSM_4_Node(int state)
{

	#ifdef DEBUGMODE
	printf("\ncurrent FSM: 4_Node\n");
	#endif

	curr_uriID = 4;
	curr_lnID = 6;

	eventCode = 0;

	switch(state)
	{
		case 0:	//START_ELEMENT, 4_Node

			#ifdef DEBUGMODE
			printf("Event: START_ELEMENT\n");
			printf("Data: 4_Node\n");
			#endif

			state = 1;
			FSM_4_Node(state);
			break;

		case 1:	//CHARACTERS, STRING

			#ifdef DEBUGMODE
			printf("Event: CHARACTERS\n");
			printf("Data: STRING\n");
			#endif

			;
			unsigned char test = 0;
			getbit(&test, 1);

			string_value = getSTRINGValue();

			state = 2;
			FSM_4_Node(state);
			break;

		case 2:	//END_ELEMENT, END_ELEMENT

			#ifdef DEBUGMODE
			printf("Event: END_ELEMENT\n");
			printf("Data: END_ELEMENT\n");
			#endif

			break;

		default:
			#ifdef DEBUGMODE
			printf("Undeclared, all stop!\n");
			#endif
			break;
	}
}

void FSM_4_NotUnderstood(int state)
{

	#ifdef DEBUGMODE
	printf("\ncurrent FSM: 4_NotUnderstood\n");
	#endif

	curr_uriID = 4;
	curr_lnID = 7;

	eventCode = 0;

	switch(state)
	{
		case 0:	//START_ELEMENT, 4_NotUnderstood

			#ifdef DEBUGMODE
			printf("Event: START_ELEMENT\n");
			printf("Data: 4_NotUnderstood\n");
			#endif

			state = 1;
			FSM_4_NotUnderstood(state);
			break;

		case 1:	//ATTRIBUTE, STRING(qname)

			#ifdef DEBUGMODE
			printf("Event: ATTRIBUTE\n");
			printf("Data: STRING(qname)\n");
			#endif

			string_value = getSTRINGValue();

			state = 2;
			FSM_4_NotUnderstood(state);
			break;

		case 2:	//END_ELEMENT, END_ELEMENT

			#ifdef DEBUGMODE
			printf("Event: END_ELEMENT\n");
			printf("Data: END_ELEMENT\n");
			#endif

			break;

		default:
			#ifdef DEBUGMODE
			printf("Undeclared, all stop!\n");
			#endif
			break;
	}
}

void FSM_4_Reason(int state)
{

	#ifdef DEBUGMODE
	printf("\ncurrent FSM: 4_Reason\n");
	#endif

	curr_uriID = 4;
	curr_lnID = 9;

	eventCode = 0;

	switch(state)
	{
		case 0:	//START_ELEMENT, 4_Reason

			#ifdef DEBUGMODE
			printf("Event: START_ELEMENT\n");
			printf("Data: 4_Reason\n");
			#endif

			state = 1;
			FSM_4_Reason(state);
			break;

		case 1:	//START_ELEMENT, 4_Text

			#ifdef DEBUGMODE
			printf("Event: START_ELEMENT\n");
			printf("Data: 4_Text\n");
			#endif

			FSM_4_Text(0);

			getbit(&eventCode, 1);
			switch(eventCode)
			{
				case 0: state = 1;
					break;
				case 1: state = 2;
					break;
			}
			FSM_4_Reason(state);
			break;

		case 2:	//END_ELEMENT, END_ELEMENT

			#ifdef DEBUGMODE
			printf("Event: END_ELEMENT\n");
			printf("Data: END_ELEMENT\n");
			#endif

			break;

		default:
			#ifdef DEBUGMODE
			printf("Undeclared, all stop!\n");
			#endif
			break;
	}
}

void FSM_4_Role(int state)
{

	#ifdef DEBUGMODE
	printf("\ncurrent FSM: 4_Role\n");
	#endif

	curr_uriID = 4;
	curr_lnID = 10;

	eventCode = 0;

	switch(state)
	{
		case 0:	//START_ELEMENT, 4_Role

			#ifdef DEBUGMODE
			printf("Event: START_ELEMENT\n");
			printf("Data: 4_Role\n");
			#endif

			state = 1;
			FSM_4_Role(state);
			break;

		case 1:	//CHARACTERS, STRING

			#ifdef DEBUGMODE
			printf("Event: CHARACTERS\n");
			printf("Data: STRING\n");
			#endif

			;
			unsigned char test = 0;
			getbit(&test, 1);

			string_value = getSTRINGValue();

			state = 2;
			FSM_4_Role(state);
			break;

		case 2:	//END_ELEMENT, END_ELEMENT

			#ifdef DEBUGMODE
			printf("Event: END_ELEMENT\n");
			printf("Data: END_ELEMENT\n");
			#endif

			break;

		default:
			#ifdef DEBUGMODE
			printf("Undeclared, all stop!\n");
			#endif
			break;
	}
}

void FSM_4_Subcode(int state)
{

	#ifdef DEBUGMODE
	printf("\ncurrent FSM: 4_Subcode\n");
	#endif

	curr_uriID = 4;
	curr_lnID = 11;

	eventCode = 0;

	switch(state)
	{
		case 0:	//START_ELEMENT, 4_Subcode

			#ifdef DEBUGMODE
			printf("Event: START_ELEMENT\n");
			printf("Data: 4_Subcode\n");
			#endif

			getbit(&eventCode, 1);
			switch(eventCode)
			{
				case 0: state = 1;
					break;
				case 1: state = 2;
					break;
			}
			FSM_4_Subcode(state);
			break;

		case 1:	//START_ELEMENT, 4_Value

			#ifdef DEBUGMODE
			printf("Event: START_ELEMENT\n");
			printf("Data: 4_Value\n");
			#endif

			FSM_4_Value(0);

			getbit(&eventCode, 1);
			switch(eventCode)
			{
				case 0: state = 0;
					break;
				case 1: state = 2;
					break;
			}
			FSM_4_Subcode(state);
			break;

		case 2:	//END_ELEMENT, END_ELEMENT

			#ifdef DEBUGMODE
			printf("Event: END_ELEMENT\n");
			printf("Data: END_ELEMENT\n");
			#endif

			break;

		default:
			#ifdef DEBUGMODE
			printf("Undeclared, all stop!\n");
			#endif
			break;
	}
}

void FSM_4_SupportedEnvelope(int state)
{

	#ifdef DEBUGMODE
	printf("\ncurrent FSM: 4_SupportedEnvelope\n");
	#endif

	curr_uriID = 4;
	curr_lnID = 13;

	eventCode = 0;

	switch(state)
	{
		case 0:	//START_ELEMENT, 4_SupportedEnvelope

			#ifdef DEBUGMODE
			printf("Event: START_ELEMENT\n");
			printf("Data: 4_SupportedEnvelope\n");
			#endif

			state = 1;
			FSM_4_SupportedEnvelope(state);
			break;

		case 1:	//ATTRIBUTE, STRING(qname)

			#ifdef DEBUGMODE
			printf("Event: ATTRIBUTE\n");
			printf("Data: STRING(qname)\n");
			#endif

			string_value = getSTRINGValue();

			state = 2;
			FSM_4_SupportedEnvelope(state);
			break;

		case 2:	//END_ELEMENT, END_ELEMENT

			#ifdef DEBUGMODE
			printf("Event: END_ELEMENT\n");
			printf("Data: END_ELEMENT\n");
			#endif

			break;

		default:
			#ifdef DEBUGMODE
			printf("Undeclared, all stop!\n");
			#endif
			break;
	}
}

void FSM_4_Text(int state)
{

	#ifdef DEBUGMODE
	printf("\ncurrent FSM: 4_Text\n");
	#endif

	curr_uriID = 4;
	curr_lnID = 14;

	eventCode = 0;

	switch(state)
	{
		case 0:	//START_ELEMENT, 4_Text

			#ifdef DEBUGMODE
			printf("Event: START_ELEMENT\n");
			printf("Data: 4_Text\n");
			#endif

			state = 1;
			FSM_4_Text(state);
			break;

		case 1:	//ATTRIBUTE, 1_lang

			#ifdef DEBUGMODE
			printf("Event: ATTRIBUTE\n");
			printf("Data: 1_lang\n");
			#endif

			string_value = getSTRINGValue();

			state = 2;
			FSM_4_Text(state);
			break;

		case 2:	//CHARACTERS, STRING

			#ifdef DEBUGMODE
			printf("Event: CHARACTERS\n");
			printf("Data: STRING\n");
			#endif

			;
			unsigned char test = 0;
			getbit(&test, 1);

			string_value = getSTRINGValue();

			state = 3;
			FSM_4_Text(state);
			break;

		case 3:	//END_ELEMENT, END_ELEMENT

			#ifdef DEBUGMODE
			printf("Event: END_ELEMENT\n");
			printf("Data: END_ELEMENT\n");
			#endif

			break;

		default:
			#ifdef DEBUGMODE
			printf("Undeclared, all stop!\n");
			#endif
			break;
	}
}

void FSM_4_Upgrade(int state)
{

	#ifdef DEBUGMODE
	printf("\ncurrent FSM: 4_Upgrade\n");
	#endif

	curr_uriID = 4;
	curr_lnID = 15;

	eventCode = 0;

	switch(state)
	{
		case 0:	//START_ELEMENT, 4_Upgrade

			#ifdef DEBUGMODE
			printf("Event: START_ELEMENT\n");
			printf("Data: 4_Upgrade\n");
			#endif

			state = 1;
			FSM_4_Upgrade(state);
			break;

		case 1:	//START_ELEMENT, 4_SupportedEnvelope

			#ifdef DEBUGMODE
			printf("Event: START_ELEMENT\n");
			printf("Data: 4_SupportedEnvelope\n");
			#endif

			FSM_4_SupportedEnvelope(0);

			getbit(&eventCode, 1);
			switch(eventCode)
			{
				case 0: state = 1;
					break;
				case 1: state = 2;
					break;
			}
			FSM_4_Upgrade(state);
			break;

		case 2:	//END_ELEMENT, END_ELEMENT

			#ifdef DEBUGMODE
			printf("Event: END_ELEMENT\n");
			printf("Data: END_ELEMENT\n");
			#endif

			break;

		default:
			#ifdef DEBUGMODE
			printf("Undeclared, all stop!\n");
			#endif
			break;
	}
}

void FSM_4_Value(int state)
{

	#ifdef DEBUGMODE
	printf("\ncurrent FSM: 4_Value\n");
	#endif

	curr_uriID = 4;
	curr_lnID = 17;

	eventCode = 0;

	switch(state)
	{
		case 0:	//START_ELEMENT, 4_Value

			#ifdef DEBUGMODE
			printf("Event: START_ELEMENT\n");
			printf("Data: 4_Value\n");
			#endif

			getbit(&eventCode, 2);
			switch(eventCode)
			{
				case 0: state = 1;
					break;
				case 1: state = 2;
					break;
				case 2: state = 3;
					break;
				case 3: state = 4;
					break;
			}
			FSM_4_Value(state);
			break;

		case 1:	//ATTRIBUTE_GENERIC, ATTRIBUTE_GENERIC

			#ifdef DEBUGMODE
			printf("Event: ATTRIBUTE_GENERIC\n");
			printf("Data: ATTRIBUTE_GENERIC\n");
			#endif

			FSM_Helper();

			getbit(&eventCode, 2);
			switch(eventCode)
			{
				case 0: state = 1;
					break;
				case 1: state = 2;
					break;
				case 2: state = 3;
					break;
				case 3: state = 4;
					break;
			}
			FSM_4_Value(state);
			break;

		case 2:	//START_ELEMENT_GENERIC, START_ELEMENT_GENERIC

			#ifdef DEBUGMODE
			printf("Event: START_ELEMENT_GENERIC\n");
			printf("Data: START_ELEMENT_GENERIC\n");
			#endif

			FSM_Helper();

			getbit(&eventCode, 2);
			switch(eventCode)
			{
				case 0: state = 2;
					break;
				case 1: state = 3;
					break;
				case 2: state = 4;
					break;
			}
			FSM_4_Value(state);
			break;

		case 3:	//END_ELEMENT, END_ELEMENT

			#ifdef DEBUGMODE
			printf("Event: END_ELEMENT\n");
			printf("Data: END_ELEMENT\n");
			#endif

			break;

		case 4:	//CHARACTERS_GENERIC, STRING

			#ifdef DEBUGMODE
			printf("Event: CHARACTERS_GENERIC\n");
			printf("Data: STRING\n");
			#endif

			string_value = getSTRINGValue();

			getbit(&eventCode, 2);
			switch(eventCode)
			{
				case 0: state = 2;
					break;
				case 1: state = 3;
					break;
				case 2: state = 4;
					break;
			}
			FSM_4_Value(state);
			break;

		default:
			#ifdef DEBUGMODE
			printf("Undeclared, all stop!\n");
			#endif
			break;
	}
}


void FSM_Start()
{
	#ifdef DEBUGMODE

	//updating String Tables...

	String uri;
	unsigned int uriID;

	String lname;
	unsigned int lnID;

	lname.str = (unsigned char*)"qname";
	lname.length = (unsigned int)5;
	lnID = LocalNames_Table_insertEntry(uriTable->entries[0].lnTable, lname);

	uri.str = (unsigned char*)"http://www.w3.org/2003/05/soap-envelope";
	uri.length = (unsigned int)39;
	uriID = URI_Table_insertEntry(uriTable, uri);

	lname.str = (unsigned char*)"Body";
	lname.length = (unsigned int)4;
	lnID = LocalNames_Table_insertEntry(uriTable->entries[uriID].lnTable, lname);

	lname.str = (unsigned char*)"Code";
	lname.length = (unsigned int)4;
	lnID = LocalNames_Table_insertEntry(uriTable->entries[uriID].lnTable, lname);

	lname.str = (unsigned char*)"Detail";
	lname.length = (unsigned int)6;
	lnID = LocalNames_Table_insertEntry(uriTable->entries[uriID].lnTable, lname);

	lname.str = (unsigned char*)"Envelope";
	lname.length = (unsigned int)8;
	lnID = LocalNames_Table_insertEntry(uriTable->entries[uriID].lnTable, lname);

	lname.str = (unsigned char*)"Fault";
	lname.length = (unsigned int)5;
	lnID = LocalNames_Table_insertEntry(uriTable->entries[uriID].lnTable, lname);

	lname.str = (unsigned char*)"Header";
	lname.length = (unsigned int)6;
	lnID = LocalNames_Table_insertEntry(uriTable->entries[uriID].lnTable, lname);

	lname.str = (unsigned char*)"Node";
	lname.length = (unsigned int)4;
	lnID = LocalNames_Table_insertEntry(uriTable->entries[uriID].lnTable, lname);

	lname.str = (unsigned char*)"NotUnderstood";
	lname.length = (unsigned int)13;
	lnID = LocalNames_Table_insertEntry(uriTable->entries[uriID].lnTable, lname);

	lname.str = (unsigned char*)"NotUnderstoodType";
	lname.length = (unsigned int)17;
	lnID = LocalNames_Table_insertEntry(uriTable->entries[uriID].lnTable, lname);

	lname.str = (unsigned char*)"Reason";
	lname.length = (unsigned int)6;
	lnID = LocalNames_Table_insertEntry(uriTable->entries[uriID].lnTable, lname);

	lname.str = (unsigned char*)"Role";
	lname.length = (unsigned int)4;
	lnID = LocalNames_Table_insertEntry(uriTable->entries[uriID].lnTable, lname);

	lname.str = (unsigned char*)"Subcode";
	lname.length = (unsigned int)7;
	lnID = LocalNames_Table_insertEntry(uriTable->entries[uriID].lnTable, lname);

	lname.str = (unsigned char*)"SupportedEnvType";
	lname.length = (unsigned int)16;
	lnID = LocalNames_Table_insertEntry(uriTable->entries[uriID].lnTable, lname);

	lname.str = (unsigned char*)"SupportedEnvelope";
	lname.length = (unsigned int)17;
	lnID = LocalNames_Table_insertEntry(uriTable->entries[uriID].lnTable, lname);

	lname.str = (unsigned char*)"Text";
	lname.length = (unsigned int)4;
	lnID = LocalNames_Table_insertEntry(uriTable->entries[uriID].lnTable, lname);

	lname.str = (unsigned char*)"Upgrade";
	lname.length = (unsigned int)7;
	lnID = LocalNames_Table_insertEntry(uriTable->entries[uriID].lnTable, lname);

	lname.str = (unsigned char*)"UpgradeType";
	lname.length = (unsigned int)11;
	lnID = LocalNames_Table_insertEntry(uriTable->entries[uriID].lnTable, lname);

	lname.str = (unsigned char*)"Value";
	lname.length = (unsigned int)5;
	lnID = LocalNames_Table_insertEntry(uriTable->entries[uriID].lnTable, lname);

	lname.str = (unsigned char*)"detail";
	lname.length = (unsigned int)6;
	lnID = LocalNames_Table_insertEntry(uriTable->entries[uriID].lnTable, lname);

	lname.str = (unsigned char*)"faultcode";
	lname.length = (unsigned int)9;
	lnID = LocalNames_Table_insertEntry(uriTable->entries[uriID].lnTable, lname);

	lname.str = (unsigned char*)"faultcodeEnum";
	lname.length = (unsigned int)13;
	lnID = LocalNames_Table_insertEntry(uriTable->entries[uriID].lnTable, lname);

	lname.str = (unsigned char*)"faultreason";
	lname.length = (unsigned int)11;
	lnID = LocalNames_Table_insertEntry(uriTable->entries[uriID].lnTable, lname);

	lname.str = (unsigned char*)"reasontext";
	lname.length = (unsigned int)10;
	lnID = LocalNames_Table_insertEntry(uriTable->entries[uriID].lnTable, lname);

	lname.str = (unsigned char*)"subcode";
	lname.length = (unsigned int)7;
	lnID = LocalNames_Table_insertEntry(uriTable->entries[uriID].lnTable, lname);

	#endif

	//update URI- & LocalName counters

	localnamecount[0] = 1;

	uricount = 5;

	localnamecount[4] = 24;

	//set strict / non-strict mode

	set_strict_mode(1);

	#ifdef DEBUGMODE
	printf("Strict Mode: true\n\n");
	#endif

	#ifdef DEBUGMODE
	printf("Start Decoding EXI File...\n\n");
	#endif

	//read DocContent elements

	#ifdef DEBUGMODE
	printf("number of possible root elements: 6\n\n");
	#endif

	eventCode = 0;
	getbit(&eventCode, 3);

	switch(eventCode)
	{
		case 0: FSM_4_Body(0);
			break;

		case 1: FSM_4_Envelope(0);
			break;

		case 2: FSM_4_Fault(0);
			break;

		case 3: FSM_4_Header(0);
			break;

		case 4: FSM_4_NotUnderstood(0);
			break;

		case 5: FSM_4_Upgrade(0);
			break;

		default:
			break;
	}
}

